export default class BookstoreService {
  data = [
    {
      id: 1,
      title: 'JavaScript подробное руководство',
      author: 'Дэвид Флэнаган',
      coverImage: 'https://javascript.ru/files/book/cover/4a49c49ced114.png',
      price: 2
    },
    {
      id: 2,
      title: 'Eloquent JavaScript',
      author: 'Manjn Haverbake',
      coverImage: 'https://media.proglib.io/wp-uploads/2017/05/cover.png',
      price: 5
    },
  ];

  getBooks() {
    return new Promise((resolve, reject)=>{
       setTimeout(()=>{
           resolve(this.data);
       }, 700);
    });
  }
}
import React from 'react';
import BooklistItem from '../BooklistItem/BooklistItem';
import {connect} from 'react-redux';

import withBookstoreService from '../hoc/with-bookstore-service';
import './Booklist.css';
import {fetchBooks, bookAddedToCart} from '../../actions';
import compose from '../../utils';

const Booklist = ({books, onAddedToCart}) => {
  return (
    <div className="book-list">
      {books.map((book) => {
        return (
          <div key={book.id}>
            <BooklistItem
              book={book}
              onAddedToCart={()=>onAddedToCart(book.id)}
            />
          </div>
        )
      })}
    </div>
  )
};

class BooklistContainer extends React.Component {

  componentDidMount() {
    this.props.fetchBooks();
  }

  render() {
    const {books, loading, error, onAddedToCart} = this.props;

    if (loading) {
      return <p>Загрузка...</p>
    }

    if (error) {
      return <p>Ошибка!</p>
    }

    return <Booklist books={books} onAddedToCart={onAddedToCart} />
  }
}



// 2 Полученные состояния преобразовали в props(достали initialState из reducer'а)
const mapStateToProps = ( {bookList: {books, loading, error}}) => {
  return {books, loading, error}
};

const mapDispatchToProps = (dispatch, ownProps) => {
  const {bookstoreService} = ownProps;
  return {
    fetchBooks: fetchBooks(bookstoreService, dispatch),
    onAddedToCart: (id) =>dispatch(bookAddedToCart(id))
  }
};

// 1 Подключили компонент к store, чтобы взять данные
export default compose(
  withBookstoreService(),
  connect(mapStateToProps, mapDispatchToProps)
)(BooklistContainer)
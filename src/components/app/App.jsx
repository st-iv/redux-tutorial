import React from 'react';
import { Route, Switch } from 'react-router-dom';
import './App.css';
import { HomePage } from '../pages';

const App = () => {
    return (
        <Switch>
            <Route
                path="/"
                component={HomePage}
                exact
            />
            {/*<Route
                path="/cart"
                component={CartPage}
                exact
            />*/}
        </Switch>
    )
};

App.propTypes = {};

export default App;
import React from 'react';
import './BooklistItem.css'

const BooklistItem = ({ book, onAddedToCart }) => {
  const { title, author, price, coverImage } = book;
  return (
      <div className="book-list-item">
        <div className="book-cover">
          <img src={coverImage} alt="cover" />
        </div>
        <div className="book-details">
          <span className="title">{`"${title}"`}</span>
          <span className="author">{author}</span>
          <span className="price">{`${price}$`}</span>
          <button
            onClick={onAddedToCart}
            className="btn btn-info add-to-cart">
            Купить
          </button>
        </div>
      </div>
  )
};

export default BooklistItem;